FROM maven:3.8.1-jdk-11-slim

WORKDIR /usr/src/mymaven

COPY . .

RUN mvn clean install
